#include "encode.h"

static const char charset[] = {
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
  'w', 'x', 'y', 'z', '0', '1', '2', '3',
  '4', '5', '6', '7', '8', '9', '+', '/',
};

int base64_encode(int output, int input) {
  int in_count = 3;
  int out_count = 4;
  char buffer[in_count];
  int buffer_size;
  int chars_in_line = 0;
  while ( (buffer_size = read(input, buffer, in_count)) > 0){
    char encoded[out_count];

    if (buffer_size == in_count) {
      encoded[0] = charset[(buffer[0] >> 2) & 0x3F];
      encoded[1] = charset[((buffer[0] & 0x3) << 4) | ((buffer[1] & 0xF0) >> 4)];
      encoded[2] = charset[((buffer[1] & 0xF) << 2) | ((buffer[2] & 0xC0) >> 6)];
      encoded[3] = charset[buffer[2] & 0x3F];
    }
    if (buffer_size == in_count-1) {
      encoded[0] = charset[(buffer[0] >> 2) & 0x3F];
      encoded[1] = charset[((buffer[0] & 0x3) << 4) | ((buffer[1] & 0xF0) >> 4)];
      encoded[2] = charset[(buffer[1] & 0xF) << 2];
      encoded[3] = '=';
    }
    if (buffer_size == in_count-2) {
      encoded[0] = charset[(buffer[0] >> 2) & 0x3F];
      encoded[1] = charset[(buffer[0] & 0x3) << 4];
      encoded[2] = '=';
      encoded[3] = '=';
    }

    if (chars_in_line < MAX_LINE - out_count) {
      write(output, encoded, out_count);
      chars_in_line += out_count;
    } else {
      write(output, encoded, out_count);
      write(output, "\n", 1);
      chars_in_line = 0;
    }
  }
  return 0;
}