#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include "encode.h"

#ifndef MAX_LINE
#define MAX_LINE 76
#endif

#define INPUT_FILE_ERROR 60
#define INPUT_FILE_NOT_FOUND 61
#define OUTPUT_FILE_ERROR 62

#ifndef no_argument
#define no_argument 0
#endif

#ifndef required_argument
#define required_argument 1
#endif

typedef int bool;
#define true 1
#define false 0


static const char charset[] = {
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
  'w', 'x', 'y', 'z', '0', '1', '2', '3',
  '4', '5', '6', '7', '8', '9', '+', '/',
};

char extendedcharset[]={"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="};

//VER RETORNO! Error? void?
//Cambiar decodificacion
int decode(unsigned char *decoded, unsigned char* string) {
  char* e1, *e2, *e3, *e4;
  int id1, id2, id3, id4,c = 0;
  e1 = strchr(charset, string[0]);  //alternativa: crear tabla para decodificar
  e2 = strchr(charset, string[1]);
  e3 = strchr(charset, string[2]);
  e4 = strchr(charset, string[3]);
  id1 = (int)(e1 - charset);
  id2 = (int)(e2 - charset);
  id3 = string[2] == '=' ? -1 : (int)(e3 - charset);
  id4 = string[3] == '=' ? -1 : (int)(e4 - charset);

  decoded[0] = ((id1 << 2) | (id2 >> 4));
  c++;
  if (id3 >= 0) {
    decoded[1] = ((id2 << 4) | (id3 >> 2));
    c++;
  }
  if (id4 >= 0) {
    decoded[2] = ((id3 << 6) | id4);
    c++;
  }
  return c;
}


int base64decode(FILE *output, FILE* input) {
  unsigned char string[4], decoded[3];
  int i,j;
  char c;
  while (feof(input)==0){
    for (i = 0; i < 4 && feof(input)==0; i++) {
      c = fgetc (input);

      if (ferror(input) != 0) exit(ferror(input));

      if(feof(input)==0 && c ==  '\n') {
        i--;
        continue;
      }
      if ( feof(input)==0  && strchr(extendedcharset, c) == NULL) {
        exit(INPUT_FILE_ERROR);
      }
      if(feof(input)==0 && c != '\n')
        string[i]=c;
    }
    if (feof(input)==0) {
      j = decode(decoded, string);
      for (i = 0; i < j; i++) {
        if(putc( (int)(decoded[i]), output) == EOF) exit(EOF);
      }
    }
  }
  return 0;
}

void show_help(){
  printf("Usage:\n");
  printf("\t tp0 -h \n");
  printf("\t tp0 -V \n");
  printf("\t tp0 [options]\n");
  printf("Options:\n");
  printf("\t -V, --version \t Print version and quit. \n");
  printf("\t -h, --help \t Print this information and quit. \n");
  printf("\t -i, --input \t Location of the input file. \n");
  printf("\t -o, --output \t Location of the output file. \n");
  printf("\t -a, --action \t Program action: encode(default) or decode. \n");
  printf("Examples:\n");
  printf("\t tp0 -a encode -i ~/input -o ~/output \n");
  printf("\t tp0 -a decode \n");
}

void show_version(){
  printf(" tp0 - \t Version 1.0 \n");
}

int main(int argc, char * argv[]) {

  int c, index = 0, i = 0;
  bool is_encode = true;
  const char *outfilename, *filename;
  FILE *foutput, *finput;

  struct option options[] = {
    {"help", no_argument, NULL, 'h'},
    {"version", no_argument, NULL, 'V'},
    {"input", required_argument, NULL, 'i'},
    {"output", required_argument, NULL, 'o'},
    {"action", required_argument, NULL, 'a'},
  };

  foutput = stdout;
  finput  = stdin;

  while ((c = getopt_long(argc, argv, "hC:VC:i:o:a",
                                 options, &index)) != -1) {

    switch (c) {
      case 'h':
        show_help();
        break;
      case 'V':
        show_version();
        break;
      case 'a':
        if (strcmp(argv[optind],"encode") == 0)
          is_encode = true;
        if (strcmp(argv[optind++],"decode") == 0)
          is_encode = false;
        break;
      case 'i':
        filename = optarg;
        finput = fopen(filename, "rb");
        if (finput == NULL) {
          exit(INPUT_FILE_NOT_FOUND);
        }
        break;
      case 'o':
        outfilename = optarg;
        foutput = fopen(outfilename, "wb");
        if (foutput == NULL) {
          exit(OUTPUT_FILE_ERROR);
        }
        break;

    }
    i++;
  }

    if (is_encode == true) {
      int fi = fileno(finput);
      int fo = fileno(foutput);
      return base64_encode(fo, fi);
    } else
      return base64decode(foutput, finput);
}
