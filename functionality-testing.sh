#!/bin/bash

. "funciones.sh"

##########################################################
##  pruebas de consola
##########################################################

prueba_consola
# falla en lineas mayores a 76 chars porque nuestro output
# no lo corta

##########################################################
##  prueba hexdump
##########################################################
echo ''
echo ''
code_hexdump

echo ''
echo ''
code_decode_hexdump

##########################################################
## prueba coding y decoding
##########################################################
echo ''
echo ''

prueba_code_encode

##########################################################
## prueba bit a bit (x y z)
##########################################################
echo ''
echo ''

prueba_bit_a_bit

##########################################################
## prueba yes
##########################################################
echo ''
echo ''

prueba_yes_count

##########################################################
## prueba cantidad de caracteres
##########################################################
echo ''
echo ''

prueba_lenght_menor_max

##########################################################
## prueba yes + decode
##########################################################
echo ''
echo ''

prueba_yes_decode

##########################################################
##  prueba de random creciente
##########################################################
echo ''
echo ''
prueba_random_decode
echo ''
echo ''
prueba_random_creciente
echo ''
echo ''
prueba_hernan

##########################################################
## prueba de archivos
##########################################################
echo ''
echo ''

prueba_de_input_output_en_archivos

##########################################################
##  pruebas de archivo inexistente
##########################################################
echo ''
echo ''

prueba_archivo_inexistente