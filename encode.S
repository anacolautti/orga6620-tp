#include <mips/regdef.h>
#include <sys/syscall.h>	

#Argumentos
#define ENCODE_ARG1 76
#define ENCODE_ARG0	72

#Tamaño
#define SIZE 72

#SRA
#define ENCODE_RA 64
#define ENCODE_FP 60
#define ENCODE_GP 56

#LTA
#define ENCODE 			52

#define BUFFER 			44
#define CHARS_IN_LINE 	36
#define BUFFER_SIZE 	32
#define OUT_COUNT 		28
#define IN_COUNT 		24

#Constantes
#define C_IN_COUNT 	3
#define C_OUT_COUNT 4

charset:
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	74
	.byte	75
	.byte	76
	.byte	77
	.byte	78
	.byte	79
	.byte	80
	.byte	81
	.byte	82
	.byte	83
	.byte	84
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	90
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	106
	.byte	107
	.byte	108
	.byte	109
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	43
	.byte	47

	.align	2
SALTO_LINEA:
	.ascii	"\n\000"

	.text
	.align	2
	.globl	base64_encode
	.ent	base64_encode

base64_encode:
	.frame	$fp,SIZE,ra		
	
	#Bloque de código para PIC
	.set	noreorder
	.cpload	t9
	.set	reorder

	#Creo el stack frame
	subu	sp,sp,SIZE

	.cprestore ENCODE_GP

	sw		ra, ENCODE_RA(sp)
	sw		$fp,ENCODE_FP(sp)
	sw		gp, ENCODE_GP(sp)
	
	move	$fp,sp

	#Salvo los argumentos
	sw		a0,ENCODE_ARG0($fp)
	sw		a1,ENCODE_ARG1($fp)

	#Cargo las inicializaciones
	sw		sp,BUFFER($fp)
	
	li		t0,C_IN_COUNT		
	sw		t0,IN_COUNT($fp) 

	li		t0,C_OUT_COUNT
	sw		t0,OUT_COUNT($fp)

	sw		zero,CHARS_IN_LINE($fp)	

#Read del input.-
readFile:
	lw 		a0, ENCODE_ARG1($fp)
	lw 		a1, BUFFER($fp)
	lw 		a2, IN_COUNT($fp)	
	li 		v0, SYS_read
	syscall

	sw		v0,BUFFER_SIZE($fp)

	lw		t0,BUFFER_SIZE($fp)		//Redundante
	bgtz	t0,whileReadFile 		//Si es mayor a cero
	
	b		return_0  				//Return final

#Entra en el while.-
whileReadFile:
	sw		sp,48($fp)

	lw		t0,OUT_COUNT($fp)		//Trabajo con el out_count
	subu	sp,sp,t0
	addu	t1,sp,16
	sw		t1,ENCODE($fp)

#(buffer_size == in_count) 
	lw		t1,BUFFER_SIZE($fp)
	lw		t0,IN_COUNT($fp)
	bne		t1,t0,nextIf1			

#encoded[0] = charset[(buffer[0] >> 2) & 0x3F]//
	lw		t1,BUFFER($fp)
	lb		t0,0(t1)		// BUFFER[0]
	sra		t0,t0,2 		// (buffer[0] >> 2)
	andi	t0,t0,0x00ff
	andi	t0,t0,0x3f 		// [(buffer[0] >> 2) & 0x3F]
	lbu		t0,charset(t0)	// Acceso al charset
		
	lw		t2,ENCODE($fp)	// ENCODE[0]
	sb		t0,0(t2)

#encoded[1] = charset[((buffer[0] & 0x3) << 4) | ((buffer[1] & 0xF0) >> 4)]//
	lw		t1,BUFFER($fp)
	lbu		t0,0(t1)		// ( buffer[0] & 0x3)
	andi	t0,t0,0x3 		// ( buffer[0] & 0x3)
	sll		t2,t0,4
	lw		t1,BUFFER($fp)
	lb		t0,1(t1)
	andi	t0,t0,0xf0
	sra		t0,t0,4
	or		t0,t2,t0
	lbu		t0,charset(t0)	// Acceso al charset
	
	lw		t2,ENCODE($fp) 	// ENCODE[1]	
	sb		t0,1(t2)
	
#encoded[2] = charset[((buffer[1] & 0xF) << 2) | ((buffer[2] & 0xSALTO_LINEA) >> 6)]//
	lw		t1,BUFFER($fp)
	lbu		t0,1(t1)
	andi	t0,t0,0xf
	sll		t2,t0,2

	lw		t1,BUFFER($fp)
	lb		t0,2(t1)
	andi	t0,t0,0xc0
	sra		t0,t0,6
	or		t0,t2,t0
	lbu		t0,charset(t0)
	
	lw		t2,ENCODE($fp)
	sb		t0,2(t2)
	
#encoded[3] = charset[buffer[2] & 0x3F]//	
	lw		t1,BUFFER($fp)
	lbu		t0,2(t1)
	andi	t0,t0,0x3f
	lbu		t0,charset(t0)
	
	lw		t2,ENCODE($fp)
	sb		t0,3(t2)

#buffer_size == in_count-1
nextIf1:
	lw		t0,IN_COUNT($fp)
	addu	t2,t0,-1
	lw		t0,BUFFER_SIZE($fp)
	bne		t0,t2,nextIf2

#encoded[0] = charset[(buffer[0] >> 2) & 0x3F]//
	lw		t1,BUFFER($fp)
	lb		t0,0(t1)
	sra		t0,t0,2
	andi	t0,t0,0x00ff
	andi	t0,t0,0x3f
	lbu		t0,charset(t0)
	
	lw		t2,ENCODE($fp)
	sb		t0,0(t2)
	
#encoded[1] = charset[((buffer[0] & 0x3) << 4) | ((buffer[1] & 0xF0) >> 4)]//
	lw		t1,BUFFER($fp)
	lbu		t0,0(t1)
	andi	t0,t0,0x3
	sll		t2,t0,4
	
	lw		t1,BUFFER($fp)
	lb		t0,1(t1)
	andi	t0,t0,0xf0
	sra		t0,t0,4
	or		t0,t2,t0
	lbu		t0,charset(t0)
	
	lw		t2,ENCODE($fp)
	sb		t0,1(t2)

#encoded[2] = charset[(buffer[1] & 0xF) << 2]//
	lw		t1,BUFFER($fp)
	lbu		t0,1(t1)
	andi	t0,t0,0xf
	sll		t0,t0,2
	lbu		t0,charset(t0)
	
	lw		t2,ENCODE($fp)
	sb		t0,2(t2)

#encoded[3] = '='//
	li		t0,61			# 0x3d
	
	lw		t1,ENCODE($fp)
	sb		t0,3(t1)

#(buffer_size == in_count-2) {
nextIf2:
	lw		t0,IN_COUNT($fp)
	addu	t2,t0,-2
	lw		t0,BUFFER_SIZE($fp)
	bne		t0,t2,continueWrite

#encoded[0] = charset[(buffer[0] >> 2) & 0x3F]//	
	lw		t2,BUFFER($fp)
	lb		t0,0(t2)
	sra		t0,t0,2
	andi	t0,t0,0x00ff
	andi	t0,t0,0x3f
	lbu		t0,charset(t0)
	
	lw		t1,ENCODE($fp)
	sb		t0,0(t1)

#encoded[1] = charset[(buffer[0] & 0x3) << 4]//	
	lw		t2,BUFFER($fp)
	lbu		t0,0(t2)
	andi	t0,t0,0x3
	sll		t0,t0,4
	lbu		t0,charset(t0)

	lw		t1,ENCODE($fp)
	sb		t0,1(t1)

#encoded[2] = '='//
	li		t0,61			# 0x3d

	lw		t2,ENCODE($fp)
	sb		t0,2(t2)

#encoded[3] = '='//
	li		t0,61			# 0x3d

	lw		t1,ENCODE($fp)
	sb		t0,3(t1)

continueWrite:
	li		t2,ENCODE_ARG1			# 0x4c
	lw		t0,OUT_COUNT($fp)
	subu	t2,t2,t0
	lw		t0,CHARS_IN_LINE($fp)
	slt		t0,t0,t2
	beq		t0,zero,writeEndLine

	lw		a0,ENCODE_ARG0($fp)
	lw		a1,ENCODE($fp)
	lw		a2,OUT_COUNT($fp)
	li		v0,SYS_write 
	syscall

	lw		t0,CHARS_IN_LINE($fp)
	lw		t2,OUT_COUNT($fp)
	addu	t0,t0,t2
	sw		t0,CHARS_IN_LINE($fp)
	b		continueLoop

writeEndLine:
	lw		a0,ENCODE_ARG0($fp)
	lw		a1,ENCODE($fp)
	lw		a2,OUT_COUNT($fp)
	li		v0,SYS_write 
	syscall

	lw		a0,ENCODE_ARG0($fp)
	la		a1,SALTO_LINEA
	li		a2,1			# 0x1
	li		v0,SYS_write 
	syscall

	sw		zero,CHARS_IN_LINE($fp)

continueLoop:
	lw		sp,48($fp)
	b		readFile

return_0:
	lw		sp,BUFFER($fp)
	move	t0,zero
	move	sp,$fp
	lw		ra,ENCODE_RA(sp)
	lw		$fp,ENCODE_FP(sp)
	addu	sp,sp,SIZE
	j		ra

	.end	base64_encode