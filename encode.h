#ifndef ENCODE_H_
#define ENCODE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <getopt.h>
#include <unistd.h>

#ifndef MAX_LINE
#define MAX_LINE 76
#endif

extern int base64_encode(int output, int input);

#endif
