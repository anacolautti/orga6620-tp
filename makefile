PROGS  = tp
CFLAGS = -g -Wall
CC = gcc

all: $(PROGS)
	:

tp: main.c encode.S
	$(CC) $(CFLAGS) -o $@ $>

clean:
	rm -f $(PROGS) *.so *.o *.a *.core

c: 
	gcc encode.c main.c -o tp