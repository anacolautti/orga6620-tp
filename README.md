# README #

### Link overleaf ###

https://www.overleaf.com/9326309fkxqrmnfzxkh

### Para compilar tp1.c ###

* gcc tp1.c -o tp1

### Para obtener c�digo assembly (desde la VM) ###

* gcc -Wall -O0 -S -mrnames tp1.c

### Para debugear ###

1- Compilamos indic�ndole -g para realizar el debugging. 

* gcc tp1.c -o tp1 -g 

2- Activamos el debug. 

* gdb tp1

3- Ya estamos en el debug! :) 

Comandos �tiles:

* break number_of_line  
* run ## Sigue hasta el break 

* n ## Next line

Card con m�s comandos: https://web.stanford.edu/class/cs107/gdb_refcard.pdf


Uno que quiero probar para la pr�xima: 
* valgrind tp1
