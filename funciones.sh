#!/bin/bash -x

RED="\e[31m"
GREEN="\e[32m"
CYAN="\e[96m"
YELLOW="\e[93m"
DEFAULT="\e[0m"
EXEC='./tp'

. "test-variables"

############################################################################
##  pruebas de consola
############################################################################

function consola {
  echo -n $1 | $EXEC
}

function prueba_consola {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA DE PARAMETROS POR CONSOLA $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"

  for INDEX in "${!ENCODE[@]}";
  do
#    echo -e "$CYAN${ENCODE[$INDEX]}$DEFAULT debe codificarse $CYAN ${DECODE[$INDEX]} $DEFAULT"
    ACTUAL_OUTPUT=$(consola ${ENCODE[$INDEX]})
#    echo -e "Obtenemos: $YELLOW $ACTUAL_OUTPUT $DEFAULT"
    if [[ "$ACTUAL_OUTPUT" == "${DECODE[$INDEX]}" ]]; then
      echo -e "$GREEN\0PASSED $DEFAULT:\necho -n ${ENCODE[$INDEX]} | $EXEC$DEFAULT \t=\t $GREEN $ACTUAL_OUTPUT $DEFAULT"
    else
      echo -e "$RED\0NOT EQUAL $DEFAULT:\necho -n ${ENCODE[$INDEX]} | $EXEC$DEFAULT \t=\t $YELLOW $ACTUAL_OUTPUT $DEFAULT"
      echo -e "Deberia ser:\n${DECODE[$INDEX]}"
    fi
  done
}


############################################################################
##  prueba hexdump
############################################################################

function hex_conversion {
  echo -n $1  | hexdump -C
}

function hex_conversion_back {
  echo -n $1  | $EXEC | $EXEC -a decode |  hexdump -C
}

function code_hexdump {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN CODE HEXDUMP (paso intermedio)$DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"

  for INDEX in "${!ENCODE[@]}";
  do
    #echo -e "Comparamos ${ENCODE[$INDEX]} con su encode + decode en hexdump $DEFAULT"
    if [[ "$(hex_conversion ${DECODE[$INDEX]})" == "$(consola ${ENCODE[$INDEX]} | hexdump -C)" ]]; then
      echo -e "$GREEN EQUAL $DEFAULT:"
      echo -e "echo -n ${ENCODE[$INDEX]} | $EXEC | hexdump -C\n$GREEN$(hex_conversion ${DECODE[$INDEX]}) $DEFAULT"
    else
      echo -e "$CYAN NOT EQUAL $DEFAULT:"
      echo -e "echo -n ${ENCODE[$INDEX]} | $EXEC | hexdump -C\n$CYAN$(consola ${ENCODE[$INDEX]} | hexdump -C) $DEFAULT"
      echo -e "Deberia ser:\n$(hex_conversion ${DECODE[$INDEX]})"

    fi
  done
}

function code_decode_hexdump {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN CODE Y DECODE HEXDUMP $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"
  echo 'echo -n Man  | $EXEC | $EXEC -a decode |  hexdump -C'

  for INDEX in "${!ENCODE[@]}";
  do
    #echo -e "Comparamos ${ENCODE[$INDEX]} con su encode + decode en hexdump $DEFAULT"
    if [[ "$(hex_conversion ${ENCODE[$INDEX]})" == "$(hex_conversion_back ${ENCODE[$INDEX]})" ]]; then
      echo -e "$GREEN PASSED $DEFAULT: $CYAN${ENCODE[$INDEX]}$DEFAULT \t=\n$GREEN $(hex_conversion ${ENCODE[$INDEX]}) $DEFAULT"
    else
      echo -e "$RED NOT EQUAL $DEFAULT: $CYAN${ENCODE[$INDEX]}$DEFAULT \t=\n$YELLOW$(hex_conversion_back ${ENCODE[$INDEX]}) $DEFAULT"
      echo -e "Deberia ser:\n$(hex_conversion ${ENCODE[$INDEX]})"

    fi
  done
}

############################################################################
## prueba coding y decoding
############################################################################
function code_decode {
  echo $1 | $EXEC | $EXEC -a decode
}

function prueba_code_encode {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN CODE Y DECODE $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"
  echo "echo TEXTO | $EXEC | $EXEC -a decode"

  for INDEX in "${!ENCODE[@]}";
  do
    ACTUAL_OUTPUT=$(code_decode ${ENCODE[$INDEX]})
#    echo -e "Comparamos ${ENCODE[$INDEX]} con su encode + decode"
    if [[ "$ACTUAL_OUTPUT" == "${ENCODE[$INDEX]}" ]]; then
      echo -e "$GREEN PASSED $DEFAULT: $CYAN ${ENCODE[$INDEX]} $DEFAULT"
    else
      echo -e "$RED NOT EQUAL $DEFAULT : $CYAN${ENCODE[$INDEX]}$DEFAULT\t!=\t$YELLOW$ACTUAL_OUTPUT$DEFAULT"
    fi
  done
}

############################################################################
## prueba bit a bit (x y z)
############################################################################
function bit_a_bit {
  echo $1 | $EXEC | $EXEC -a decode | od -t c
}

function prueba_bit_a_bit {

  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA BIT A BIT $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"

  echo 'echo TEXTO | $EXEC | $EXEC -a decode | od -t c'

  for INDEX in "${!ENCODE[@]}";
  do
    ACTUAL_OUTPUT=$(bit_a_bit ${ENCODE[$INDEX]})
    COMPARE=$(echo "${ENCODE[$INDEX]}" | od -t c)
    #echo -e "Comparamos ${ENCODE[$INDEX]} bit a bit"
    if [[ "$ACTUAL_OUTPUT" == "$COMPARE" ]]; then
      echo -e "$GREEN PASSED $DEFAULT: $CYAN${ENCODE[$INDEX]}$DEFAULT"
    else
      echo -e "$RED NOT EQUAL $DEFAULT: $CYAN${ENCODE[$INDEX]}$DEFAULT\t!=\n$YELLOW$ACTUAL_OUTPUT$DEFAULT"
      echo -e "Deberia ser:\n$COMPARE)"
    fi
  done
}
############################################################################
## prueba yes
############################################################################
function yes_test {
  yes | head -c 1024 | $EXEC -a encode
}

function prueba_yes_count {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA YES $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"


  RESULT=$(yes_test ENCODE[$INDEX]})
  LENGHT=$(echo $RESULT | wc --max-line-length)

  if [ $LENGHT -lt $MAXCHARS ]; then
    echo -e "$RED\0Menos de $MAXCHARS chars.$DEFAULT"
  else
    IFS=$'\n' read -rd '' -a y <<<"${DECODE[$INDEX]}"
    LENGHT=$(echo $y | wc --max-line-length) 
    if [ $LENGHT -eq $MAXCHARS ]; then
      echo -e "$GREEN\0$A los $MAXCHARS chars corta. $DEFAULT"
    else
      echo -e "$RED\0$La linea mas larga mide $LENGHT caracteres.$DEFAULT"
    fi
  fi

}

############################################################################
## prueba de cantidad de caracteres no superior a MAXCHARS
############################################################################
function prueba_lenght_menor_max {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA LENGHT $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"

  for INDEX in "${!ENCODE[@]}";
  do
    RESULT=$(consola ${ENCODE[$INDEX]})
    #echo $RESULT
    LENGHT=$(echo $RESULT | wc --max-line-length)
    #echo $LENGHT

    if [ $LENGHT -lt $MAXCHARS ]; then
      echo -e "${ENCODE[$INDEX]}: No aporta (menos de $MAXCHARS chars)"
    else
      IFS=$'\n' read -rd '' -a y <<<"$RESULT"
      LENGHT=$(echo $y | wc --max-line-length) 
      if [ $LENGHT -eq $MAXCHARS ]; then
        echo -e "$GREEN\0${ENCODE[$INDEX]}: A los $MAXCHARS chars corta. $DEFAULT"
      else
        echo -e "$RED\0${ENCODE[$INDEX]}: La linea mas larga mide $LENGHT caracteres.$DEFAULT"
      fi
    fi
  done

}


############################################################################
## prueba yes + decode
############################################################################

function prueba_yes_decode {
  echo ''
  echo ''
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA YES + DECODE $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"


  for INDEX in "${!ENCODE[@]}";
  do
    ACTUAL_OUTPUT=$(yes | head -c 1024 | $EXEC -a encode | $EXEC -a decode | wc -c)
    if [[ "$ACTUAL_OUTPUT" == "1024" ]]; then
      echo -e "$GREEN PASSED. ENCODE + DECODE + wc = 1024 $DEFAULT"
    else
      echo -e "$RED NOT EQUAL. ENCODE + DECODE + wc != 1024  $DEFAULT"
      echo -e "RESULT IS $ACTUAL_OUTPUT"
    fi
  done
}

############################################################################
##  pruebas de random
############################################################################

function prueba_random_decode {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA RANDOM DECODE $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"

  n=1;
  while [ $n -lt 10 ]; do
    m=$((n*2))
    head -c $m /dev/urandom > random.original;
    $EXEC -a decode -i random.original -o /tmp/out.bin;
    #$EXEC -a decode -i test-result -o /tmp/out.bin;
    if [ $? -eq "60" ];
    then
      echo -e "$GREEN PASSED. Rechazó archivo inválido "
    else
      echo -e "$RED NOT EQUAL. No rechazó el archivo. Chequear si random original era válido:"
    fi
    echo -e "$(cat random.original | hexdump -C)$DEFAULT"
    n=$((n+1));
    rm -f /tmp/test.bin /tmp/out.b64 /tmp/out.bin

  done

}

function prueba_random_creciente {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA RANDOM CRECIENTE $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"

  n=1;
  while [ $n -lt 20 ]; do
    head -c $n /dev/urandom > /tmp/in.bin
    $EXEC -a encode -i /tmp/in.bin -o /tmp/out.b64
    $EXEC -a decode -i /tmp/out.b64 -o /tmp/out.bin
    if diff /tmp/in.bin /tmp/out.bin; then :; else
      echo ERROR: $n;

      echo -e "$YELLOW\0in.bin$DEFAULT\0:\n$(cat in.bin | hexdump -C)"
      echo -e "$YELLOW\0out.bin$DEFAULT\0:\n$(cat out.bin | hexdump -C)"

      break;
    fi
    echo ok: $n;
    n=$((n+1));
    rm -f /tmp/in.bin /tmp/out.b64 /tmp/out.bin
  done
  
}

function prueba_hernan {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA QUE MANDO HERNAN AL GRUPO $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"

  n=1
  while [ $n -lt 40 ]; do
    head -c $n </dev/urandom >in.bin;
    $EXEC -a encode -i in.bin -o out.b64;
    base64 in.bin > out-ref.b64;
    if diff -b out.b64 out-ref.b64; then :; else
      echo ERROR encoding: $n;
      break;
    fi

    $EXEC -a decode -i out.b64 -o out.bin;
    if diff in.bin out.bin; then :; else
      echo ERROR decoding: $n;
      echo -e "$YELLOW\0in.bin$DEFAULT\0:\n$(cat in.bin | hexdump -C)"
      echo -e "$YELLOW\0out.bin$DEFAULT\0:\n$(cat out.bin | hexdump -C)"

      break;
    fi
    echo ok: $n;
    n="`expr $n + 1`";
    rm -f in.bin out.b64 out.bin out-ref.b64;
  done
}

############################################################################
## prueba de archivos
############################################################################

function archivo {
  $EXEC -a encode -i $1 -o $2 # > /dev/null
}

function prueba_de_input_output_en_archivos {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA DE ARCHIVOS ENTRADA Y SALIDA $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"
  echo '$EXEC -a encode -i INPUT_FILE -o OUTPUT_FILE'
  echo ''
  for INDEX in "${!INPUT_FILES[@]}";
  do
    rm ${OUTPUT_FILES[$INDEX]}

    echo "diff ${RESULT_FILES[$INDEX]} encoded(${INPUT_FILES[$INDEX]})"
    echo ''
    archivo ${INPUT_FILES[$INDEX]} ${OUTPUT_FILES[$INDEX]}


    RESULT=$(diff ${RESULT_FILES[$INDEX]} ${OUTPUT_FILES[$INDEX]})

    if [ "$RESULT" != "" ];
    then
      echo ''
      echo -e "$RED NOT EQUAL $DEFAULT"
      echo ''
      echo "diff entre los archivos devuelve:"
      echo -e "$YELLOW $RESULT $DEFAULT"
    else
      echo -e "$GREEN PASSED $DEFAULT"
    fi
    echo ''
  done
}

function prueba_archivo_inexistente {
  echo -e "$CYAN############################################################################$DEFAULT"
  echo -e "$CYAN PRUEBA DE ARCHIVOS ENTRADA Y SALIDA $DEFAULT"
  echo -e "$CYAN############################################################################$DEFAULT"
  echo '$EXEC -a encode -i [inexistente]'
  echo ''
  if [ "$($EXEC -a encode -i inexistente)" != "61" ];
  then
    echo -e "$GREEN PASSED $DEFAULT"
  else
    echo -e "$RED NOT PASSED $DEFAULT"
  fi

}